﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


[RequireComponent( typeof(TextMesh) )]
public class DamageText : MonoBehaviour
{
    //[SerializeField]
    float _speed = 10f;

    Camera _mainCam;
    float _scale = 0.3f;
    float _deadTime = 1f;
    float _awakeTime;

    TextMesh _text;

    void Awake()
    {
        _awakeTime = Time.time;
        _mainCam = Camera.main;
        Assert.IsNotNull(_mainCam);
        _text = GetComponent<TextMesh>();
    }

    void Update()
    {
        MoveUp();
        LookAtCamera();
        Scale();
        DeadCheck();
        Color();
    }

    private void Color()
    {
        Color c = _text.color;
        c.a = _text.color.a - Time.deltaTime;
        _text.color = c;
    }

    private void DeadCheck()
    {
        if (_awakeTime + _deadTime < Time.time)
            Destroy(gameObject);
    }

    private void Scale()
    {
        _scale += Time.deltaTime;
        transform.localScale = new Vector3(_scale, _scale, _scale);
    }

    private void LookAtCamera()
    {
        this.transform.LookAt(_mainCam.transform);
        transform.forward = transform.forward * -1;
    }

    private void MoveUp()
    {
        this.transform.Translate(0f, Time.deltaTime * _speed, 0f, Space.World);
    }
}
