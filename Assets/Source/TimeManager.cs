﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class TimeManager : MonoBehaviour
{
    [SerializeField]
    private float _timeSpeed = 1f;

    void Update()
    {
        Time.timeScale = _timeSpeed;
    }
}
