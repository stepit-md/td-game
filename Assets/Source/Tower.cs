﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;




public class Tower : MonoBehaviour
{
    private static float damageAmmount = 12f;

    [SerializeField]
    private float _radius;
    [SerializeField]
    private Transform _fireFrom;


    [Range(3, 128), SerializeField]
    private int _steps = 3;

    [SerializeField] private float _maxRotateRads = 1f;


    private Enemy _activeEnemy;
    private Weapon _gun;
    private Vector3 _gunDefaultAimPos;



    void Awake()
    {
        FindSelfGun();
        InitRadiusRangeLine();
    }

    private void InitRadiusRangeLine()
    {
        var lr = this.GetComponent<LineRenderer>();
        Assert.IsNotNull(lr);

        if (_gun == null)
        {
            lr.enabled = false;
            return;
        }

        lr.enabled = true;
        var positions = GenerateCirclePoints(_radius, _steps);
        lr.positionCount = positions.Length;
        lr.SetPositions(positions);
    }

    void Start()
    {
        InitGunOrientation();
    }


    void Update()
    {
        ForgetActiveEnemyIfTooFar();
        ForgetActiveEnemyIfIsNotAlive();

        if (_activeEnemy == null)
            ScanForEnemies();

        if (_activeEnemy != null)
            AimGun(_activeEnemy.transform.position);

        if (_activeEnemy != null && _gun.canFire && this.IsEnemyInSight())
            TrigerWeapon();

        if (_activeEnemy == null)
            AimGun(_gunDefaultAimPos);
    }

    private void ForgetActiveEnemyIfIsNotAlive()
    {
        if (_activeEnemy == null) return;
        if (!_activeEnemy.IsAlive)
            _activeEnemy = null;
    }

    private bool IsEnemyInSight()
    {
        if (_activeEnemy == null)
            return false;

        Vector3 gunFwd = _gun.gunForward;
        Vector3 enemyDir = _activeEnemy.transform.position - _gun.transform.position;
        enemyDir.Normalize();
        float dot = Vector3.Dot(gunFwd, enemyDir);
        return dot > _gun.aimDotTreshold;
    }

    private void AimGun(Vector3 targetPos)
    {
        Transform gunXf = _fireFrom;
        Vector3 targetDirection = (targetPos - _gun.transform.position);
        targetDirection.Normalize();
        // rotate gun here
        Vector3 newRotation = Vector3.RotateTowards(_gun.transform.forward, targetDirection, _maxRotateRads * Time.deltaTime, 1f);
        _gun.transform.forward = newRotation;
    }


    private void InitGunOrientation()
    {
        var go = GameObject.FindGameObjectWithTag("EnemyPath");
        Assert.IsNotNull(go);
        Path path = go.GetComponent<Path>();
        Vector3 point = path.GetClosestWaipointPos(transform.position);
        _gunDefaultAimPos = point;
    }

    private void FindSelfGun()
    {
        _gun = this.transform.GetComponentInChildren<Weapon>();
        if (_gun == null)
            this.enabled = false;
    }

    private void ForgetActiveEnemyIfTooFar()
    {
        if (_activeEnemy != null)
        {
            if (IsFireRange(_activeEnemy) == false)
                _activeEnemy = null;
        }
    }

    private void ScanForEnemies()
    {
        for (int i = 0; i < Spawner.EnemyList.Count; i++)
        {
            Enemy current = Spawner.EnemyList[i];
            if (IsFireRange(current))
            {
                _activeEnemy = current;
                break;
            }
        }
    }

    private void TrigerWeapon()
    {
        _gun.Fire(_activeEnemy);
    }


    private bool IsFireRange(Enemy e)
    {
        Assert.IsNotNull(e);
        Vector3 towerPos = this.transform.position;
        float distance = (e.transform.position - towerPos).magnitude;
        return distance < _radius;
    }


    void OnDrawGizmos()
    {
        var arr = GenerateCirclePoints(_radius, _steps);
        Gizmos.matrix = this.transform.localToWorldMatrix;
        for (int i = 0; i < arr.Length -1; ++i)
            Gizmos.DrawLine(arr[i], arr[i + 1]);
        Gizmos.DrawLine(arr[arr.Length - 1], arr[0]);
    }


    private Vector3[] GenerateCirclePoints(float radius, int steps)
    {
        float angle = (Mathf.PI * 2f) / steps;
        var arr = new Vector3[steps];

        for (int i = 0; i < steps; i++)
        {
            float currAngle = i * angle;
            float x = Mathf.Sin(currAngle) * radius;
            float y = Mathf.Cos(currAngle) * radius;

            Vector3 nextPos = new Vector3(x, 0.2f, y);
            arr[i] = nextPos;
        }
        return arr;
    }

}
