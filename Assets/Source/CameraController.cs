﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Vector2 _speed = Vector2.one;
    [SerializeField]
    private float _zoomSpeed = 1f;

    [SerializeField]
    private float _minSize = 1f, _maxSize = 10f;


    private Vector3 _lastMousePos;



    void Awake()
    {
        _lastMousePos = Input.mousePosition;
    }

    private void Update()
    {
        ProcessScrollWheel();

        if (Input.GetMouseButton(1))
        {
            Vector3 diffMousePos = _lastMousePos - Input.mousePosition;

            float offsetX = diffMousePos.x * _speed.x;
            float offsetY = diffMousePos.y * _speed.y;

            this.transform.Translate(offsetX, 0f, 0f, Space.Self);
            // calculate forward vector
            Vector3 frwOffsetVec = this.transform.forward;
            // project it on XZ plane
            frwOffsetVec.y = 0f;
            // make length 1 again
            frwOffsetVec.Normalize();
            // apply delta Y
            frwOffsetVec = frwOffsetVec * offsetY;
            // move object
            this.transform.localPosition += frwOffsetVec;
        }

        _lastMousePos = Input.mousePosition;
    }

    private void ProcessScrollWheel()
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        float offset = scroll * _zoomSpeed;
        float newSize = Camera.main.orthographicSize - offset;
        newSize = Mathf.Clamp(newSize, _minSize, _maxSize);
        Camera.main.orthographicSize = newSize;
    }
}
