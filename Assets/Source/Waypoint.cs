﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    [SerializeField]
    private bool _drawGizmos = true;

    public Vector3 wpos => this.transform.position;


    void OnDrawGizmos()
    {
        if (_drawGizmos == false)
            return;
        Vector3 wpos = this.transform.position;

        float lineWidth = 0.8f;
        // red line
        //Gizmos.color = Color.red;
        //Vector3 p1 = new Vector3(wpos.x + lineWidth, wpos.y, wpos.z);
        //Vector3 p2 = new Vector3(wpos.x - lineWidth, wpos.y, wpos.z);
        //Gizmos.DrawLine(p1, p2);
        //// green line
        //Gizmos.color = Color.green;
        //p1 = new Vector3(wpos.x, wpos.y + lineWidth, wpos.z);
        //p2 = new Vector3(wpos.x, wpos.y - lineWidth, wpos.z);
        //Gizmos.DrawLine(p1, p2);
        //// blue line
        //Gizmos.color = Color.blue;
        //p1 = new Vector3(wpos.x, wpos.y, wpos.z + lineWidth);
        //p2 = new Vector3(wpos.x, wpos.y, wpos.z - lineWidth);
        //Gizmos.DrawLine(p1, p2);
        // draw sphere
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(wpos, 0.1f);
        // draw cube
        Color c = Color.cyan;
        c.a = 0.2f;
        Gizmos.color = c;
        Gizmos.DrawWireCube(wpos, Vector3.one);
    }
}