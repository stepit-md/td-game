﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;




public class Enemy : MonoBehaviour
{
    const string PATH_TAG = "EnemyPath";
    const float MAX_HEALTH = 100f;

    private Path _path;
    private int _currWaypointId;
    private float _health = MAX_HEALTH;
    [SerializeField] Transform _healthBarXf;
    [SerializeField] Transform _shootTargetXf;
    [SerializeField] TextMesh _textPrefab;

    public float speed;

    public bool IsAlive { get { return _health > 0f; } }
    public Vector3 ShootTarget { get { return _shootTargetXf.position; } }




    void Start()
    {
        InitEnemyPath();
        SetPositionToFirstWaypoint();
    }


    void Update()
    {
        int nextWaypointId = _currWaypointId + 1;
        if (nextWaypointId >= _path.allWaypoints.Length) // check if we reached end
        {
            Debug.Log("<color=orange>Reached Finish</color> normal text");
            Spawner.EnemyList.Remove(this);
            Object.Destroy(this.gameObject);
            return;
        }

        // check
        Vector3 currPos = this.transform.position;
        Vector3 targetPos = _path.allWaypoints[nextWaypointId].transform.position;
        Vector3 newPos = Vector3.MoveTowards(currPos, targetPos, speed * Time.deltaTime);

        this.transform.position = newPos;
        if (newPos == targetPos)
        {
            _currWaypointId++;
            int nextIndex = _currWaypointId + 1;
            if (nextIndex < _path.allWaypoints.Length)
            {
                this.transform.LookAt(_path.allWaypoints[nextIndex].transform);
            }
        }

        UpdateHealthBarOrientation();
    }

    private void InitEnemyPath()
    {
        var go = GameObject.FindGameObjectWithTag(PATH_TAG);
        Assert.IsNotNull(go);
        _path = go.GetComponent<Path>();
        Assert.IsNotNull(_path);
    }

    private void SetPositionToFirstWaypoint()
    {
        Waypoint wp = _path.allWaypoints[0];
        Vector3 wpPosition = wp.transform.position;
        this.transform.position = wpPosition;
        this.transform.LookAt(_path.allWaypoints[_currWaypointId].transform);
        _currWaypointId = 0;
    }

    private void UpdateHealthBar()
    {
        float healt = _health / MAX_HEALTH;
        _healthBarXf.localScale = new Vector3(healt, 1f, 1f);
    }

    private void UpdateHealthBarOrientation()
    {
        Vector3 fwd = Camera.main.transform.position - _healthBarXf.parent.position;
        fwd.y = 0f;
        fwd.Normalize();
        Quaternion q = Quaternion.LookRotation(fwd, Vector3.up);
        _healthBarXf.parent.rotation = q;
    }

    private void CheckIfDestroyed()
    {
        if (_health <= 0f)
        {
            Destroy(this.gameObject, 3f);
            speed = 0f;
            Spawner.EnemyList.Remove(this);
        }
    }


    public void TakeDamege(float ammount)
    {
        if (ammount == 0f) return;
        _health -= ammount;
        UpdateHealthBar();
        CheckIfDestroyed();
        SpawnDamageTextInfo(ammount);
    }


    private void SpawnDamageTextInfo(float ammount)
    {
        TextMesh t = Instantiate(_textPrefab);
        Debug.Log(t.name);
        t.transform.position = ShootTarget;
        t.transform.LookAt(Camera.main.transform.localPosition);
        t.transform.localScale = Vector3.zero;
        t.text = ammount.ToString();
    }
}


