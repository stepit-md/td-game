﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;



public class Spawner : MonoBehaviour
{
    static private int EnemyCount;
    static public List<Enemy> EnemyList = new List<Enemy>();

    [SerializeField]
    GameObject _prototype;
    [SerializeField]
    Vector2 _speedRange;


    private void Spawn()
    {
        EnemyCount++;
        GameObject go = Instantiate(_prototype);
        Enemy enemy = go.GetComponent<Enemy>();
        enemy.speed = Random.Range(_speedRange.x, _speedRange.y);
        EnemyList.Add(enemy);
    }

    //void OnValidate()
    //{

    //}


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {   this.Spawn(); }
    }
}
