using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class RoketProjectile : MonoBehaviour
{
    float _riseTime = 1f;
    [SerializeField]
    AnimationCurve _curve;
    [SerializeField]
    float _explodeRadius = 1f;
    [SerializeField]
    float _exlodeDamege = 20f;

    float _maxRiseHight;
    float _speed = 15f;

    Enemy _target;
    float _startTime;
    Vector3 _startPos;
    Vector3 _startDirection;

    Vector3 _enemyPos;
    Vector3 _vector;
    Vector3 _localTarget;

    public void Setup(Enemy target)
    {
        _maxRiseHight = transform.position.y + 1f;


        _target = target;
        _startTime = Time.time;
        _startPos = this.transform.position;
        _startDirection = this.transform.forward;

        _enemyPos = target.transform.position;
        _enemyPos.y = transform.position.y;
        _vector = _enemyPos - transform.position;
    }


    void Update()
    {
        if (_target != null)
            _localTarget = _target.transform.position;

        float normalizedTime = (Time.time - _startTime) / _riseTime;

        float s = _curve.Evaluate(normalizedTime);
        Vector3 pos = _startPos;



        if (normalizedTime < 0.6f)
        {
            pos.y += s * _maxRiseHight;
            pos += _vector.normalized * _vector.magnitude * normalizedTime;
        }
        else
            pos = Vector3.MoveTowards(transform.position, _localTarget, (_speed += 3f) * Time.deltaTime);


        Vector3 lastPos = this.transform.position;
        this.transform.position = pos;
        // change projectile orientation
        Vector3 dir = (pos - lastPos).normalized;
        transform.forward = dir;

        if (transform.position == _localTarget)
            Explode();
    }


    private void Explode()
    {
        Destroy(gameObject);
        Enemy[] arr = Spawner.EnemyList.ToArray();
        Vector3 roketPos = this.transform.position;

        for (int i = 0; i < arr.Length; ++i)
        {
            Enemy e = arr[i];
            Vector3 pos = e.transform.position;
            float dist = (pos - roketPos).magnitude;

            dist = Mathf.Min(dist, _explodeRadius);
            float normDist = 1f - dist / _explodeRadius;
            float damege = normDist * _exlodeDamege;

            e.TakeDamege(damege);
        }
    }
}
