﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;





public class KineticW : Weapon
{
    [SerializeField] private BulletProjectile _bulletPrefab;
    [SerializeField] private float _bulletSpeed = 1f;


    [SerializeField] private int _magCapacity = 3;
    [SerializeField] private float _reloadCooldown = 2f;

    [SerializeField] private float _recoilOffset;
    [SerializeField] private float _barrelSpeed = 1f;
    [SerializeField] private Transform _barrelXf;

    private int _bulletsCount = 0;
    private float _reloadFinishedTime;
    private Vector3 _barrelStartPosition;


    void Awake()
    {
        _bulletsCount = _magCapacity;
        _barrelStartPosition = _barrelXf.localPosition;
    }

    void Update()
    {
        Vector3 lpos = Vector3.MoveTowards(
            _barrelXf.localPosition, 
            _barrelStartPosition, 
            _barrelSpeed * Time.deltaTime);
        _barrelXf.localPosition = lpos;
    }

    public override void Fire(Enemy enemy)
    {
        _bulletsCount--;
        base.Fire(enemy);
        enemy.TakeDamege(_damege);
        TrowProjectile(enemy);
        RecoilBarrel();

        if (_bulletsCount <= 0)
            ReloadWeapon();
    }

    private void ReloadWeapon()
    {
        _bulletsCount = _magCapacity;
        _reloadFinishedTime = Time.time + _reloadCooldown;
    }

    private void RecoilBarrel()
    {
        _barrelXf.Translate(0f, -_recoilOffset, 0f, Space.Self);
    }

    private void TrowProjectile(Enemy targetEnemy)
    {
        BulletProjectile bulletObj = GameObject.Instantiate(_bulletPrefab);
        bulletObj.transform.SetParent(bulletSpanXf, false);
        bulletObj.transform.localPosition = Vector3.zero;
        bulletObj.Setup(targetEnemy, _bulletSpeed);
    }


    private bool isReloading => Time.time < _reloadFinishedTime;
    public override bool canFire => base.canFire && isReloading == false;
}