﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;




public class Weapon : MonoBehaviour
{
    [SerializeField]
    protected float _damege = 1f;


    [SerializeField]
    private float _coolDown = 1f;

    [SerializeField]
    protected Transform _bulletSpanXf;


    protected float _lastFireTime;



    // fields
    public virtual bool canFire => _lastFireTime + _coolDown < Time.time;
    public virtual Vector3 gunForward => _bulletSpanXf.forward;
    public virtual float aimDotTreshold => 0.98f;
    public Transform bulletSpanXf => _bulletSpanXf;


    public virtual void Fire(Enemy enemy)
    {
        Assert.IsTrue(canFire, "can't call weapon fire during cooldown");
        //enemy.TakeDamege(_damege);
        _lastFireTime = Time.time;
    }


}
