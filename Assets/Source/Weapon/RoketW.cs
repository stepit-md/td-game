﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class RoketW : Weapon
{
    [SerializeField]
    Transform _bodyXf;
    [SerializeField]
    GameObject _projectilePrefab;

   


    public override Vector3 gunForward => this.transform.forward;
    public override float aimDotTreshold => 0.6f;

    public override void Fire(Enemy enemy)
    {
        base.Fire(enemy);
        TrowProjectile(enemy);
    }

    void Update()
    {
        Vector3 aimDirection = this.transform.forward;
        aimDirection.y *= -1f;
        _bodyXf.forward = aimDirection;
    }

    private void TrowProjectile(Enemy enemy)
    {
        GameObject go = Instantiate(_projectilePrefab);
        go.transform.position = _bulletSpanXf.position;
        go.transform.forward = this.bulletSpanXf.forward;
        var cmp = go.GetComponent<RoketProjectile>();
        Assert.IsNotNull(cmp);
        cmp.Setup(enemy);
    }
}
