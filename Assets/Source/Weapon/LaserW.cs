﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class LaserW : Weapon
{
    [SerializeField]
    private LineRenderer _line;

    private Vector3[] _pointArr;


    public override bool canFire => true;

    void Awake()
    {
        _line.enabled = false;
        _line.positionCount = 2;
        _pointArr = new Vector3[2];
    }

    //void OnRenderObject()
    //{
    //    _line.enabled = false;
    //}


    public override void Fire(Enemy enemy)
    {
        base.Fire(enemy);
        enemy.TakeDamege(_damege * Time.deltaTime);
        // visual
        _line.enabled = true;
        _pointArr[0] = this.bulletSpanXf.position;
        _pointArr[1] = enemy.ShootTarget;
        _line.SetPositions(_pointArr);
        this.StartCoroutine(DisableLineAtEndOfFrame());
    }

    IEnumerator DisableLineAtEndOfFrame()
    {
        yield return new WaitForEndOfFrame();
        _line.enabled = false;
    }

}
