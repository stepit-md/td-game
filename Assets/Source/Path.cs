﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;




public class Path : MonoBehaviour
{
    [NonSerialized]
    public Waypoint[] allWaypoints;

    [SerializeField]
    private Color _lineColor = Color.green;
    [SerializeField]
    private Color _startColor = Color.green;
    [SerializeField]
    private Color _endColor = Color.red;

    private int _childCout;


    void Awake()
    {
        InitWaypoitsArray();
    }


    public Vector3 GetClosestWaipointPos(Vector3 origin)
    {
        //Waypoint closestWP = allWaypoints[0];
        float minDist = float.MaxValue;
        Vector3 point = allWaypoints[0].wpos;
        for (int i = 0; i < allWaypoints.Length; i++)
        {
            float dist = (origin - allWaypoints[i].wpos).magnitude;
            if (dist < minDist)
            {
                minDist = dist;
                point = allWaypoints[i].wpos;
            }
        }
        return point;
    }

    private void InitWaypoitsArray()
    {
        int num = this.transform.childCount;

        if (allWaypoints != null && _childCout == num)
            return;

        _childCout = num;
        allWaypoints = new Waypoint[num];
        for (int i = 0; i < num; i++)
        {
            Transform childXf = this.transform.GetChild(i);
            Waypoint wp = childXf.GetComponent<Waypoint>();
            Assert.IsNotNull(wp, "component not found");
            allWaypoints[i] = wp;
        }
    }



    void OnDrawGizmos()
    {
        InitWaypoitsArray();
        Gizmos.color = _lineColor;
        for (int i = 0; i < allWaypoints.Length -1; i++)
        {
            var p1 = allWaypoints[i].transform.position;
            var p2 = allWaypoints[i+1].transform.position;
            Gizmos.DrawLine(p1, p2);
        }

        float radius = 1f;
        Gizmos.color = _startColor;
        Gizmos.DrawWireSphere(allWaypoints[0].transform.position, radius);
        Gizmos.color = _endColor;
        Waypoint last = allWaypoints[allWaypoints.Length - 1];
        Gizmos.DrawWireSphere(last.transform.position, radius);
    }
}
