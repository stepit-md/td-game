﻿Shader "Unlit/laser-beam"
{
    Properties
    {
        _Opacity ("My opacity param", Range(0,1)) = 1
		_GradPower ("Gradient Power", float) = 1
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 color : COLOR;
            };


			float _Opacity, _GradPower;


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float v = i.uv.y;
				v = v * 2; // range [0, 2]
				v = v - 1; // range [-1, 1]
				v = abs(v);// range [1,1]
				v = 1 - v;
				//v = pow(v, _GradPower);
				v *= _Opacity;

				float4 vcol = i.color;
				return fixed4(vcol.rgb, v);
            }
            ENDCG
        }
    }
}
