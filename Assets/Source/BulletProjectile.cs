﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BulletProjectile : MonoBehaviour
{
    float _speed;
    Enemy _enemy;
    Vector3 _targetPos;

    public void Setup(Enemy enemy, float speed)
    {
        _enemy = enemy;
        _speed = speed;
        _targetPos = _enemy.ShootTarget;
        this.transform.LookAt(_enemy.transform);
    }


    void Update()
    {
        if (_enemy != null)
            _targetPos = _enemy.ShootTarget;

        Vector3 pos = Vector3.MoveTowards(transform.position, _targetPos, _speed * Time.deltaTime);
        this.transform.position = pos;

        if (pos == _targetPos)
        {
            Destroy(this.gameObject);
        }
    }
}
